#include <stdio.h>
#include <stdlib.h>
#include "listacontas.h"
void menu()
{
  printf("\n ------------------------------------------- ");
  printf("\n ------ Digite a opcao e aperte enter ------ ");
  printf("\n -------------------------------------------\n ");
  printf("\n 1. Inserir uma conta bancaria. ");
  printf("\n 2. Inserir uma conta poupanca. ");
  printf("\n 3. Inserir uma conta fidelidade. ");
  printf("\n 4. Realizar credito. ");
  printf("\n 5. Realizar debito. ");
  printf("\n 6. Consultar saldo. ");
  printf("\n 7. Consultar bonus de conta. ");
  printf("\n 8. Realizar transferencia entre duas contas. ");
  printf("\n 9. Render juros de uma conta poupanca. ");
  printf("\n 10. Render bonus de uma conta fidelidade. ");
  printf("\n 11. Remover uma conta. ");
  printf("\n 12. Imprimir numero e saldo de toda as contas. ");
  printf("\n 13. Sair. \n ");

}


int main()
{
  int opcao = 0;
  int num;
  int num2;
  float saldo;
  float valor;
  float bonus;
  float control;
  //Cria uma lista vazia
  Lista* lst = lst_cria();

  while(opcao != 13)
  {
    menu();

    scanf("%d",&opcao);

    switch(opcao)
    {
      case 1:
      {
        Lista* conta;
        printf("\nDigite o numero da conta: ");
        scanf("%d",&num);
        printf("\nDigite o saldo da conta: ");
        scanf("%f",&saldo);
        conta = cria_conta_bancaria(num,saldo);
        lst = lst_insere(lst,conta);
        printf("\nInserido com sucesso!\n");
        break;
      }
      case 2:
      {
        Lista* conta;
        printf("\nDigite o numero da conta: ");
        scanf("%d",&num);
        printf("\nDigite o saldo da conta: ");
        scanf("%f",&saldo);
        conta = cria_conta_poupanca(num,saldo);
        lst = lst_insere(lst,conta);
        printf("\nInserido com sucesso!\n");
        break;
      }
      case 3:
      {
        Lista* conta;
        printf("\nDigite o numero da conta: ");
        scanf("%d",&num);
        printf("\nDigite o saldo da conta: ");
        scanf("%f",&saldo);
        conta = cria_conta_fidelidade(num,saldo,0.0);
        lst = lst_insere(lst,conta);
        printf("\nInserido com sucesso!\n");
        break;
      }
      case 4:
      {
        printf("\nDigite o numero da conta para creditar: ");
        scanf("%d",&num);
        printf("\nDigite o valor: ");
        scanf("%f",&valor);
        inserir_saldo_conta(lst,num,valor);
        break;
      }
      case 5:
      {
        printf("\nDigite o numero da conta para debitar: ");
        scanf("%d",&num);

        if(remover_saldo_conta(lst,num,valor))
          printf("\nInserido com sucesso!\n");
        else
          printf("\nConta inexistente ou valor maior que o saldo!\n");
        break;
      }
      case 6:
      {
        printf("\nDigite o numero da conta para consultar saldo: ");
        scanf("%d",&num);
        saldo = consultar_saldo_conta(lst,num);
        if(saldo == -1)
          printf("\nConta inexistente!\n");
        else
          printf("\nSaldo da conta = %f\n",saldo);
        break;
      }
      case 7:
      {
        printf("\nDigite o numero da conta fidelidade  para consultar bonus: ");
        scanf("%d",&num);
        bonus = consultar_bonus_conta(lst,num);
        if(bonus == -1)
          printf("\nConta inexistente!\n");
        else if (bonus == -2)
          printf("\nNao eh uma conta fidelidade.");
        else
          printf("\nBonus da conta = %f\n",bonus);
        break;
      }
      case 8:
      {
        printf("\nDigite o numero da conta a ser debitado: ");
        scanf("%d",&num);
        printf("\nDigite o numero da conta a ser creditado: ");
        scanf("%d",&num2);
        printf("\nDigite o valor: ");
        scanf("%f",&valor);
        control = transferencia_conta(lst,num,num2,valor);

        if(control == 0)
          printf("\nConta inexistente!\n");
        else if (control == -1)
          printf("\nSaldo insuficiente.");
        else
          printf("\nTransferencia concluida!");
        break;
      }
      case 9:
      {
        printf("\nDigite o numero da conta poupanca para render juros: ");
        scanf("%d",&num);
        printf("\nInsira os juros: ");
        scanf("%f",&valor);
        control = render_juros_conta(lst,num,valor);
        if(control == -1)
          printf("\nConta inexistente!\n");
        else if (control == -2)
          printf("\nNao eh uma conta poupanca.");
        else
          printf("\nJuros rendidos com sucesso!\n");
        break;
      }
      case 10:
      {
        printf("\nDigite o numero da conta fidelidade para render bonus: ");
        scanf("%d",&num);
        control = render_bonus_conta(lst,num);
        if(control == -1)
          printf("\nConta inexistente!\n");
        else if (control == -2)
          printf("\nNao eh uma conta fidelidade.");
        else
          printf("\nBonus rendidos com sucesso!\n");
        break;
      }
      case 11:
      {
        printf("\nDigite o numero da conta para remover: ");
        scanf("%d",&num);
        lst = lst_remove(lst,num);
        break;
      }
      case 12:
      {
        printf("\nImprimindo num e bonus de todas as contas\n");
        lst_imprime(lst);
        break;
      }
    }

  }


  /*int var;

  //Cria os ponteiros de listas
  Lista* lst;
  Lista* lst2;
  Lista* conta1;
  Lista* conta2;
  Lista* conta3;
  Lista* conta4;
  Lista* conta5;
  Lista* conta6;

  //Cria uma lista vazia
  lst = lst_cria();

  conta1 = cria_conta_bancaria(1,10.0);
  conta2 = cria_conta_poupanca(2,20.0);
  conta3 = cria_conta_fidelidade(3,30.0,0.0);

  //Insere elementos no inicio
  lst = lst_insere(lst,conta1);
  lst = lst_insere(lst,conta2);
  lst = lst_insere(lst,conta3);


  //Imprime os valores armazenados
  printf("Imprime a lista:\n");
  lst_imprime(lst);
  printf("\n");

  //Imprime os valores armazenados com recursao
  printf("Imprime a lista com  recursao:\n");
  lst_imprime_rec(lst);
  printf("\n");

  //Imprime os valores em ordem reversa
  printf("Imprime os valores em ordem reversa:\n");
  lst_imprime_rev(lst);
  printf("\n");

  //Verifica se a lista esta vazia
  if(!lst_vazia(lst))
    printf("Lista nao vazia!\n");
  else
    printf("Lista vazia!\n");

  lst2 = lst_cria();

  conta4 = cria_conta_bancaria(1,10.0);
  conta5 = cria_conta_poupanca(2,20.0);
  conta6 = cria_conta_fidelidade(3,30.0,0.0);

  //Insere elementos no inicio
  lst2 = lst_insere(lst2,conta4);
  lst2 = lst_insere(lst2,conta5);
  lst2 = lst_insere(lst2,conta6);

  var = 2;
  lst2 = lst_busca(lst2,var);
  if(lst2 != NULL)
    printf("Conta com numero %d encontrado!\n",var);
  else
    printf("Elemento com numero %d nao encontrado!\n",var);


  //remove um elemento da lista sem recursao
  lst = lst_remove(lst,var);
  printf("\nImprime valores apos remocao da conta de numero %d\n",var);
  lst_imprime(lst);

  //remove um elemento da lista com recurs�o
  var = 1;
  lst = lst_remove_rec(lst,var);
  printf("\nImprime valores apos remocao da conta %d com recursao\n",var);
  lst_imprime(lst);

  //libera a lista
  lst_libera(lst);*/

  return 0;
}
