#ifndef LISTACONTAS_H_INCLUDED
#define LISTACONTAS_H_INCLUDED

#include <stdlib.h>
#include <stdio.h>

#define BANCARIA 0
#define POUPANCA 1
#define FIDELIDADE 2

typedef struct lista Lista;

typedef struct conta_bancaria Conta_bancaria;

typedef struct conta_poupanca Conta_poupanca;

typedef struct conta_fidelidade Conta_fidelidade;

/** Cria lista vazia*/
Lista* lst_cria(void);

/** Verifica se a lista ta vazia */
int lst_vazia(Lista* lst);

/** Cria um n� com conta bancaria */
Lista* cria_conta_bancaria (int num, float saldo);

/** Cria um n� com conta poupanca */
Lista* cria_conta_poupanca (int num, float saldo);

/** Cria um n� com conta fidelidade*/
Lista* cria_conta_fidelidade (int num, float saldo, float bonus);

/** Insere novo elemento na cabe�a da lista */
Lista* lst_insere (Lista* lst, Lista* lst_ins);

/** Insere saldo em uma conta */
int inserir_saldo_conta(Lista* lst, int num, float valor);

/** remove saldo em uma conta */
int remover_saldo_conta(Lista* lst, int num, float valor);

/** Consulta saldo de uma conta */
float consultar_saldo_conta(Lista* lst, int num);

int transferencia_conta(Lista* lst, int num1, int num2, float valor);

int render_juros_conta(Lista* lst,int num, float juros);

int render_bonus_conta(Lista* lst,int num);

/** Imprime numero de conta de cada no da lista */
void lst_imprime (Lista* lst);

/** Imprime lista de maneira recursiva*/
void lst_imprime_rec (Lista* lst);

/** Imprime lista em ordem reversa*/
void lst_imprime_rev (Lista* lst);

/** Busca um elemento na lista, retorna null caso n�o encontre*/
Lista* lst_busca (Lista* lst, int val);

/** Remove elemento da lista*/
Lista* lst_remove (Lista* lst, int val);

/** Remove elemento da lista de maneira recursiva*/
Lista* lst_remove_rec (Lista* lst, int val);

/** Libera a lista completa */
void lst_libera (Lista* lst);

/**Verifica se duas listas s�o iguais**/
int lst_igual (Lista* lst1, Lista* lst2);

#endif // LISTACONTAS_H_INCLUDED
