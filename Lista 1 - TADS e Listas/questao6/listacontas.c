#include "listacontas.h"

struct lista {
  int tipo;
  void* info;
  struct lista* prox;
};

struct conta_bancaria {
  int num;
  float saldo;
};

struct conta_poupanca {
  int num;
  float saldo;
};

struct conta_fidelidade {
  int num;
  float saldo;
  float bonus;
};

Lista* lst_cria(void)
{
  return NULL;
}

int lst_vazia(Lista* lst)
{
  return (lst == NULL);
}

static void conta_libera(Lista* conta)
{
  free(conta->info);
  free(conta);
}

Lista* cria_conta_bancaria (int num, float saldo)
{
    Conta_bancaria* conta;
    Lista* lst;

    conta = (Conta_bancaria*) malloc(sizeof(Conta_bancaria));
    conta->num = num;
    conta->saldo = saldo;

    lst = (Lista*) malloc(sizeof(Lista));
    lst->tipo = BANCARIA;
    lst->info = conta;
    lst->prox = NULL;

    return lst;
}

Lista* cria_conta_poupanca (int num, float saldo)
{
    Conta_poupanca* conta;
    Lista* lst;

    conta = (Conta_poupanca*) malloc(sizeof(Conta_poupanca));
    conta->num = num;
    conta->saldo = saldo;

    lst = (Lista*) malloc(sizeof(Lista));
    lst->tipo = POUPANCA;
    lst->info = conta;
    lst->prox = NULL;

    return lst;
}

Lista* cria_conta_fidelidade (int num, float saldo, float bonus)
{
    Conta_fidelidade* conta;
    Lista* lst;

    conta = (Conta_fidelidade*) malloc(sizeof(Conta_fidelidade));
    conta->num = num;
    conta->saldo = saldo;
    conta->bonus = bonus;

    lst = (Lista*) malloc(sizeof(Lista));
    lst->tipo = FIDELIDADE;
    lst->info = conta;
    lst->prox = NULL;

    return lst;
}

static int get_num_bancaria(Conta_bancaria* conta)
{
    return conta->num;
}

static int get_num_poupanca(Conta_poupanca* conta)
{
    return conta->num;
}

static int get_num_fidelidade(Conta_fidelidade* conta)
{
    return conta->num;
}

static float get_saldo_bancaria(Conta_bancaria* conta)
{
    return conta->saldo;
}

static float get_saldo_poupanca(Conta_poupanca* conta)
{
    return conta->saldo;
}

static float get_saldo_fidelidade(Conta_fidelidade* conta)
{
    return conta->saldo;
}

static float get_bonus_fidelidade(Conta_fidelidade* conta)
{
    return conta->bonus;
}

static void add_saldo_bancaria(Conta_bancaria* conta, float valor)
{
    conta->saldo += valor;
}

static void add_saldo_poupanca(Conta_poupanca* conta, float valor)
{
    conta->saldo += valor;
}

static void add_saldo_fidelidade(Conta_fidelidade* conta, float valor)
{
    conta->saldo += valor;
    conta->bonus += valor/100.0;
}

static int remove_saldo_bancaria(Conta_bancaria* conta, float valor)
{
  if(valor > conta->saldo)
    return 0;
  else
    conta->saldo -= valor;
  return 1;
}

static int remove_saldo_poupanca(Conta_poupanca* conta, float valor)
{
  if(valor > conta->saldo)
    return 0;
  else
    conta->saldo -= valor;
  return 1;
}

static int remove_saldo_fidelidade(Conta_fidelidade* conta, float valor)
{
  if(valor > conta->saldo)
    return 0;
  else
    conta->saldo -= valor;
  return 1;
}

static float get_bonus(Conta_fidelidade* conta)
{
  return conta->bonus;
}

static void render_juros(Conta_poupanca* conta, float juros)
{
  conta->saldo += conta->saldo*(juros/100);
}

static void render_bonus(Conta_fidelidade* conta)
{
  conta->saldo += conta->bonus;
  conta->bonus = 0;
}


static int get_num(Lista* lst)
{
  int num;

  switch (lst->tipo)
  {
    case BANCARIA:
      num = get_num_bancaria((Conta_bancaria*)lst->info);
      break;
    case POUPANCA:
      num = get_num_poupanca((Conta_poupanca*)lst->info);
      break;
    case FIDELIDADE:
      num = get_num_fidelidade((Conta_fidelidade*)lst->info);
      break;
  }

  return num;
}

static float get_saldo(Lista* lst)
{
  float saldo;

  switch (lst->tipo)
  {
    case BANCARIA:
      saldo = get_saldo_bancaria((Conta_bancaria*)lst->info);
      break;
    case POUPANCA:
      saldo = get_saldo_poupanca((Conta_poupanca*)lst->info);
      break;
    case FIDELIDADE:
      saldo = get_saldo_fidelidade((Conta_fidelidade*)lst->info);
      break;
  }

  return saldo;
}

static void inserir_saldo(Lista* lst, float valor)
{
  switch (lst->tipo)
  {
    case BANCARIA:
      add_saldo_bancaria((Conta_bancaria*)lst->info,valor);
      break;
    case POUPANCA:
      add_saldo_poupanca((Conta_poupanca*)lst->info,valor);
      break;
    case FIDELIDADE:
      add_saldo_fidelidade((Conta_fidelidade*)lst->info,valor);
      break;
  }
}

static int retirar_saldo(Lista* lst, float valor)
{
  switch (lst->tipo)
  {
    case BANCARIA:
      return remove_saldo_bancaria((Conta_bancaria*)lst->info,valor);
      break;
    case POUPANCA:
      return remove_saldo_poupanca((Conta_poupanca*)lst->info,valor);
      break;
    case FIDELIDADE:
      return remove_saldo_fidelidade((Conta_fidelidade*)lst->info,valor);
      break;
  }
}

Lista* lst_insere (Lista* lst, Lista* lst_ins)
{
  int num_insere;
  int num_atual;

  if(lst_ins == NULL)
    return lst;

  if(lst == NULL)
    return lst_ins;

  num_insere = get_num(lst_ins);
  num_atual = get_num(lst);

  if( num_insere < num_atual)
  {
    lst_ins->prox = lst;
    return lst_ins;
  }
  else
  {
    Lista* aux_ant = (Lista*) malloc(sizeof(Lista));
    Lista* aux_prox = (Lista*) malloc(sizeof(Lista));

    aux_ant = lst;
    aux_prox = lst->prox;

    while(aux_prox != NULL)
    {
      num_atual = get_num(aux_prox);
      if(num_insere > num_atual)
      {
        aux_ant = aux_prox;
        aux_prox = aux_prox->prox;
      }
      else
        break;
    }

    aux_ant->prox = lst_ins;
    lst_ins->prox = aux_prox;

    return lst;
  }
}

void lst_imprime (Lista* lst)
{
  int num;
  float saldo;
  float bonus;

  while(lst != NULL)
  {
    num = get_num(lst);
    saldo = get_saldo(lst);
    if(lst->tipo == FIDELIDADE)
    {
      bonus = get_bonus_fidelidade(lst->info);
      printf("tipo:fidelidade -- numero_conta = %d numero_saldo = %f numero_bonus=%f\n",num,saldo,bonus);
    }
    else if(lst->tipo == BANCARIA)
      printf("tipo:bancaria -- numero_conta = %d numero_saldo = %f\n",num,saldo);
    else if(lst->tipo == POUPANCA)
      printf("tipo:poupanca -- numero_conta = %d numero_saldo = %f\n",num,saldo);

    lst = lst->prox;
  }
}

void lst_imprime_rec (Lista* lst)
{
  int num;
  float saldo;
  float bonus;

  if(!lst_vazia(lst))
  {
    num = get_num(lst);
    saldo = get_saldo(lst);
    if(lst->tipo == FIDELIDADE)
    {
      bonus = get_bonus_fidelidade(lst);
      printf("tipo:fidelidade -- numero_conta = %d numero_saldo = %f numero_bonus=%f\n",num,saldo,bonus);
    }
    else if(lst->tipo == BANCARIA)
      printf("tipo:bancaria -- numero_conta = %d numero_saldo = %f\n",num,saldo);
    else if(lst->tipo == POUPANCA)
      printf("tipo:poupanca -- numero_conta = %d numero_saldo = %f\n",num,saldo);

    lst_imprime_rec(lst->prox);
  }
}

void lst_imprime_rev (Lista* lst)
{
  int num;
  float saldo;
  float bonus;

  if(!lst_vazia(lst))
  {
    lst_imprime_rev(lst->prox);

    num = get_num(lst);
    saldo = get_saldo(lst);
    if(lst->tipo == FIDELIDADE)
    {
      bonus = get_bonus_fidelidade(lst);
      printf("tipo:fidelidade -- numero_conta = %d numero_saldo = %f numero_bonus=%f\n",num,saldo,bonus);
    }
    else if(lst->tipo == BANCARIA)
      printf("tipo:bancaria -- numero_conta = %d numero_saldo = %f\n",num,saldo);
    else if(lst->tipo == POUPANCA)
      printf("tipo:poupanca -- numero_conta = %d numero_saldo = %f\n",num,saldo);

  }
}

Lista* lst_busca (Lista* lst, int val)
{
  int num;

  while(lst != NULL)
  {
    num = get_num(lst);
    if(num == val)
      return lst;
    lst = lst->prox;
  }

  return NULL;
}

int inserir_saldo_conta(Lista* lst, int num, float valor)
{
  Lista* aux = lst_busca(lst,num);
  if(aux == NULL)
    return 0;
  else
    inserir_saldo(aux,valor);
  return 1;
}

int remover_saldo_conta(Lista* lst, int num, float valor)
{
  Lista* aux = lst_busca(lst,num);
  if(aux == NULL)
    return 0;
  else
    return retirar_saldo(aux,valor);
}

int transferencia_conta(Lista* lst, int num1, int num2, float valor)
{

  Lista* aux1 = lst_busca(lst,num1);
  Lista* aux2 = lst_busca(lst,num2);
  if(aux1 == NULL || aux2 == NULL)
    return 0;
  else
  {
    if(retirar_saldo(aux1,valor) == 0)
      return -1;
    else
      inserir_saldo(aux2,valor);
  }
  return 1;

}

float consultar_saldo_conta(Lista* lst, int num)
{
  float saldo;

  Lista* aux = lst_busca(lst,num);
  if(aux == NULL)
    return -1;
  else
    saldo = get_saldo(lst);

  return saldo;
}

float consultar_bonus_conta(Lista* lst, int num)
{
  Lista* aux = lst_busca(lst,num);
  if(aux == NULL)
    return -1;
  else
    if(lst->tipo == FIDELIDADE)
      return get_bonus(lst->info);
    else
      return -2;
}

int render_juros_conta(Lista* lst,int num, float juros)
{
  Lista* aux = lst_busca(lst,num);
  if(aux == NULL)
    return -1;
  else
    if(lst->tipo == POUPANCA)
    {
      render_juros(lst->info,juros);
      return 1;
    }
    else
      return -2;
}

int render_bonus_conta(Lista* lst,int num)
{
  Lista* aux = lst_busca(lst,num);
  if(aux == NULL)
    return -1;
  else
    if(lst->tipo == FIDELIDADE)
    {
      render_bonus(lst->info);
      return 1;
    }
    else
      return -2;
}



Lista* lst_remove (Lista* lst, int val)
{
  int num;
  Lista* ant = NULL;
  Lista* aux = lst;

  while(aux != NULL)
  {
    num = get_num(aux);
    if(num == val)
        break;

    ant = aux;
    aux = aux->prox;
  }

  if(aux == NULL)
    return lst;
  if(ant == NULL)
    lst = aux->prox;
  else
    ant->prox = aux->prox;

  conta_libera(aux);
  return lst;

}

Lista* lst_remove_rec (Lista* lst, int val)
{
  int num;

  if(lst == NULL)
    return lst;

  num = get_num(lst);

  if(num == val)
  {
    Lista* aux = lst->prox;
    conta_libera(lst);
    return aux;
  }

  lst->prox = lst_remove_rec(lst->prox,val);

  return lst;

}

void lst_libera (Lista* lst)
{
  while(lst != NULL)
  {
    Lista* conta = lst->prox;
    conta_libera(lst);
    lst = conta;
  }
}

int lst_igual (Lista* lst1, Lista* lst2)
{
  while(lst1 != NULL && lst2 != NULL)
  {
    if(lst1->info != lst2->info)
      return 0;
    else
    {
      lst1 = lst1->prox;
      lst2 = lst2->prox;
    }
  }

  return (lst1 == lst2);
}
