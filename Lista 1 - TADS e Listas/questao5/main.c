#include <stdio.h>
#include <stdlib.h>
#include "listacirculardup.h"

int main()
{

  int var;
  Lista* lst;
  Lista* lst2;
  Lista* lst3;

  //Cria uma lista vazia
  lst = lst_cria();

  //Insere elementos no inicio
  lst = lst_insere(lst,2);
  lst = lst_insere(lst,5);
  lst = lst_insere(lst,7);

  //Imprime os valores armazenados
  printf("Imprime os valores:\n");
  lst_imprime(lst);
  printf("\n");

  //Imprime os valores usando recursao
  printf("Imprime os valores com recursao:\n");
  lst_imprime_rec(lst);
  printf("\n");

  //Imprime os valores em ordem reversa
  printf("Imprime os valores em ordem reversa:\n");
  lst_imprime_rev(lst);
  printf("\n");

  //Verifica se a lista esta vazia
  if(!lst_vazia(lst))
    printf("Lista nao vazia!\n");
  else
    printf("Lista vazia!\n");

  lst2 = lst_cria();

  //Insere elementos no inicio
  lst2 = lst_insere(lst2,2);
  lst2 = lst_insere(lst2,5);
  lst2 = lst_insere(lst2,7);

  var = 5;
  lst2 = lst_busca(lst2,var);
  if(lst2 != NULL)
    printf("Elemento %d encontrado!\n",var);
  else
    printf("Elemento %d nao encontrado!\n",var);


  //remove um elemento da lista sem recursao
  lst = lst_remove(lst,var);
  printf("\nImprime valores apos remocao do %d\n",var);
  lst_imprime(lst);

  lst = lst_insere(lst,8);
  lst = lst_insere(lst,9);
  lst = lst_insere(lst,10);

  printf("\nImprime a nova lista \n");
  lst_imprime(lst);

  //remove um elemento da lista com recurs�o
  var = 8;
  lst = lst_remove_rec(lst,var);
  printf("\nImprime valores apos remocao do %d com recursao\n",var);
  lst_imprime(lst);

  lst3 = lst_cria();

  lst3 = lst_insere(lst3,2);
  lst3 = lst_insere(lst3,7);
  lst3 = lst_insere(lst3,9);
  lst3 = lst_insere(lst3,10);

  printf("\n-----------------------------\n",var);
  printf("\nImprime lista 1\n");
  lst_imprime(lst);
  printf("\n");
  printf("\nImprime lista 3\n");
  lst_imprime(lst3);

  printf("\nVerificar se as listas sao iguais:\n");
  if(lst_igual(lst,lst3))
    printf("Sao iguais\n");
  else
    printf("Nao sao iguais\n");

  //libera a lista
  lst_libera(lst);

  return 0;
}

