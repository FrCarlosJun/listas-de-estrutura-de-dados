#include "listacirculardup.h"

struct lista
{
  int info;
  struct lista* prox;
  struct lista* ant;
};


Lista* lst_cria (void)
{
  return NULL;
}

int lst_vazia(Lista* lst)
{
  return (lst == NULL);
}

Lista* lst_insere (Lista* lst, int val)
{

  Lista* new_lst = (Lista*) malloc(sizeof(Lista));
  new_lst->info = val;
  if(lst == NULL)
  {
    new_lst->prox = new_lst;
    new_lst->ant = new_lst;
    return new_lst;
  }
  else
  {
    Lista* aux = (Lista*) malloc(sizeof(Lista));
    aux = lst;
    while(aux->prox != lst)
        aux = aux->prox;
    aux->prox = new_lst;
    new_lst->ant = aux;
  }

  new_lst->prox = lst;
  lst->ant = new_lst;
  return new_lst;
}

void lst_imprime (Lista* lst)
{
  Lista* aux = lst;
  if(aux) do
  {
    printf("info = %d\n",aux->info);
    aux = aux->prox;
  } while(aux != lst);
}

void lst_imprime_rec (Lista* lst)
{
  static int control = 0;
  static Lista* aux;

  if(control == 0)
  {
    aux = lst;
    control++;
  }

  if(!lst_vazia(lst))
  {
    printf("info = %d\n",lst->info);
    if(lst->prox != aux)
        lst_imprime_rec(lst->prox);
  }

  control = 0;
}

void lst_imprime_rev (Lista* lst)
{
  Lista* aux = lst;
  if(aux) do
  {
    aux = aux->ant;
    printf("info = %d\n",aux->info);
  } while(aux != lst);
}

Lista* lst_busca (Lista* lst, int val)
{
  Lista* aux = lst;
  if(aux != NULL) do
  {
    if(aux->info == val)
      return aux;
    aux = aux->prox;
  } while(aux != lst);

  return NULL;
}

Lista* lst_remove (Lista* lst, int val)
{
  if(lst == NULL)
    return NULL;

  Lista* ant = lst;
  Lista* prox = lst->prox;

  while(prox != lst && prox->info != val)
  {
    ant = prox;
    prox = prox->prox;
  }

  if(prox == lst)
  {
    if(prox->info == val)
    {
      if(prox->ant == lst)
        return NULL;
      else
      {
        ant->prox = prox->prox;
        prox->prox->ant = ant;
        lst = prox->prox;
      }
    }
  }
  if(prox->info == val)
  {
    ant->prox = prox->prox;
    prox->prox->ant = ant;

  }
  else
    return lst;

  free(prox);
  return lst;

}

Lista* lst_remove_rec (Lista* lst, int val)
{
  static int control = 0;
  static Lista* aux;

  if(lst == NULL)
    return lst;

  if(control == 0)
  {
    aux = lst;
    control++;
  }

  if(lst->info == val)
  {
    Lista* p = lst->prox;
    p->ant = lst->ant;
    //Sea elemento a ser removido for o primeiro eh necessario
    //procurar o ultimo elemento e apontar para o segundo
    if(lst == aux)
    {
        Lista* aux2 = p;
        while(aux2->prox != lst)
            aux2 = aux2->prox;
        aux2->prox = p;
        p->ant = aux2;
    }
    free(lst);
    return p;
  }

  if(lst->prox != aux)
    lst->prox = lst_remove_rec(lst->prox,val);

  control == 0;
  return lst;

}

void lst_libera (Lista* lst)
{
  if(lst == NULL)
    return;

  Lista* prox = lst->prox;
  while(lst != prox)
  {
    Lista* aux = prox;
    prox = prox->prox;
    free(aux);
  }
  free(lst);
}

int lst_igual (Lista* lst1, Lista* lst2)
{
  Lista* aux1 = lst1;
  Lista* aux2 = lst2;

  if(lst1 == NULL || lst2 == NULL)
    return (aux1 == aux2);

  do
  {
    if(aux1->info != aux2->info)
      return 0;
    else
    {
      aux1 = aux1->prox;
      aux2 = aux2->prox;
    }
  } while(aux1 != lst1 && aux2 != lst2);

  if(aux1 == lst1 && aux2 == lst2)
    return 1;
  else
    return 0;
}





