#include "listadupordenada.h"

struct lista {
  int info;
  struct lista* prox;
};

Lista* lst_cria(void)
{
  return NULL;
}

int lst_vazia(Lista* lst)
{
  return (lst == NULL);
}

Lista* lst_insere (Lista* lst, int val)
{
  Lista* new_lst = (Lista*) malloc(sizeof(Lista));

  if(lst == NULL || val < lst->info)
  {
    new_lst->info = val;
    new_lst->prox = lst;
    return new_lst;
  }
  else
  {
    Lista* aux_ant = (Lista*) malloc(sizeof(Lista));
    Lista* aux_prox = (Lista*) malloc(sizeof(Lista));

    aux_ant = lst;
    aux_prox = lst->prox;

    while(aux_prox != NULL)
    {
      if(val > aux_prox->info)
      {
        aux_ant = aux_prox;
        aux_prox = aux_prox->prox;
      }
      else
        break;
    }

    new_lst->info = val;
    aux_ant->prox = new_lst;
    new_lst->prox = aux_prox;

    return lst;

  }
}

void lst_imprime (Lista* lst)
{
  while(lst != NULL)
  {
    printf("info = %d\n",lst->info);
    lst = lst->prox;
  }
}

void lst_imprime_rec (Lista* lst)
{
  if(!lst_vazia(lst))
  {
    printf("info = %d\n",lst->info);
    lst_imprime_rec(lst->prox);
  }
}

void lst_imprime_rev (Lista* lst)
{
  if(!lst_vazia(lst))
  {
    lst_imprime_rev(lst->prox);
    printf("info = %d\n",lst->info);
  }
}

Lista* lst_busca (Lista* lst, int val)
{
  while(lst != NULL)
  {
    if(lst->info == val)
      return lst;
    lst = lst->prox;
  }

  return NULL;
}

Lista* lst_remove (Lista* lst, int val)
{
  Lista* ant = NULL;
  Lista* p = lst;

  while(p!= NULL && p->info != val)
  {
    ant = p;
    p = p->prox;
  }

  if(p == NULL)
    return lst;
  if(ant == NULL)
    lst = p->prox;
  else
    ant->prox = p->prox;

  free(p);
  return lst;

}

Lista* lst_remove_rec (Lista* lst, int val)
{

  if(lst == NULL)
    return lst;

  if(lst->info == val)
  {
    Lista* p = lst->prox;
    free(lst);
    return p;
  }

  lst->prox = lst_remove_rec(lst->prox,val);

  return lst;

}

void lst_libera (Lista* lst)
{
  while(lst != NULL)
  {
    Lista* t = lst->prox;
    free(lst);
    lst = t;
  }
}

int lst_igual (Lista* lst1, Lista* lst2)
{
  while(lst1 != NULL && lst2 != NULL)
  {
    if(lst1->info != lst2->info)
      return 0;
    else
    {
      lst1 = lst1->prox;
      lst2 = lst2->prox;
    }
  }

  return (lst1 == lst2);
}
