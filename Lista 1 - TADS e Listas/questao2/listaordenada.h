#ifndef LISTAORDENADA_H_INCLUDED
#define LISTAORDENADA_H_INCLUDED

#include <stdlib.h>
#include <stdio.h>

typedef struct lista Lista;

/** Cria lista vazia*/
Lista* lst_cria(void);

/** Verifica se a lista ta vazia */
int lst_vazia(Lista* lst);

/** Insere novo elemento na cabe�a da lista */
Lista* lst_insere (Lista* lst, int val);

/** Imprime lista */
void lst_imprime (Lista* lst);

/** Imprime lista de maneira recursiva*/
void lst_imprime_rec (Lista* lst);

/** Imprime lista em ordem reversa*/
void lst_imprime_rev (Lista* lst);

/** Busca um elemento na lista, retorna null caso n�o encontre*/
Lista* lst_busca (Lista* lst, int val);

/** Remove elemento da lista*/
Lista* lst_remove (Lista* lst, int val);

/** Remove elemento da lista de maneira recursiva*/
Lista* lst_remove_rec (Lista* lst, int val);

/** Libera a lista completa */
void lst_libera (Lista* lst);

/**Verifica se duas listas s�o iguais**/
int lst_igual (Lista* lst1, Lista* lst2);

#endif // LISTAORDENADA_H_INCLUDED
