#ifndef HASHTABLE_H_INCLUDED
#define HASHTABLE_H_INCLUDED

#include "lista.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

struct hashtable
{
  int num_buckets;
  int bits;
  int bucket_size;
  //int* bucket_map;
  struct bucket** bucket_pos;
};

struct bucket
{
  int id;
  int num_ref;
  int tam;
  Lista* pos;
};

typedef struct hashtable Hashtable;

typedef struct bucket Bucket;

/** Cria uma tabela  Has de inteiros do tamanho desejado*/
Hashtable* criar_tabela(int tam);

Bucket* criar_bucket(Hashtable* table);

/** Insere um elemento inteiro na tabela*/
Hashtable* inserir_tabela(Hashtable* tabela, int info);

/** Busca o elemento na tabela e retorna a lista do elemento*/
int buscar_tabela(Hashtable* tabela, int info);

int remover_tabela(Hashtable* tabela, int info);

void imprimir_todosBuckets(Hashtable* tabela);

/** Libera toda a tabela*/
//void liberar_tabela(Hashtable* table);

#endif // HASHTABLE_H_INCLUDED
