#include "hashtable.h"
#include "lista.h"

/*static void printBits(size_t const size, void const * const ptr)
{
    unsigned char *b = (unsigned char*) ptr;
    unsigned char byte;
    int i, j;

    for (i=size-1;i>=0;i--)
    {
        for (j=7;j>=0;j--)
        {
            byte = (b[i] >> j) & 1;
            printf("%u", byte);
        }
    }
    puts("");
}*/

static int get_bucket(int bits, int info)
{
  int bit_hash = pow(2,bits);
  int bit_mask = 0b0;
  int bit_info;
  int i;

  for(i=0;i<bits;i++)
    bit_mask += (0b1 << i);

  bit_info = info & bit_mask;

  //return *(tabela->bucket_map+(bit_info%bit_hash));
  return bit_info%bit_hash;
}

static int get_bucket_byBit(int bits, int info)
{
  int bit_mask = 0b0;
  int i;

  for(i=0;i<bits;i++)
    bit_mask += (0b1 << i);

  return info & bit_mask;
}

Bucket* criar_bucket(Hashtable* table)
{
  Bucket* bucket = NULL;
  if((bucket = malloc(sizeof(Bucket))) == NULL)
    return NULL;

  bucket->tam = 0;
  bucket->id = table->num_buckets;
  bucket->num_ref = 1;
  table->num_buckets++;
  bucket->pos = lst_cria(table->bucket_size);
  //if((bucket->pos = (Lista*) malloc(sizeof(Lista*)*bucket_size)) == NULL)
    //return NULL;

  return bucket;
}


Hashtable* criar_tabela(int bucket_size)
{
    int i;
    int init_size = 2;
    Hashtable* table = NULL;
    if((table = malloc(sizeof(Hashtable))) == NULL)
        return NULL;

    table->bucket_size = bucket_size;
    table->bits = 1;
    table->num_buckets = 0; //Dois buckets {0} e {1}

    //if((table->bucket_map = (int*) malloc(sizeof(int)*init_size)) == NULL)
        //return NULL;

    if((table->bucket_pos = (Bucket**) malloc(sizeof(Bucket*)*init_size)) == NULL)
        return NULL;

    for( i = 0; i < init_size; i++)
    {
      table->bucket_pos[i] = (Bucket*) criar_bucket(table);
      //*(table->bucket_map+i) = i;
    }


    return table;
}

Hashtable* realloc_bucket(Hashtable* table,int f_pos)
{
  printf("\n realocando tabela -");
  int i;
  int size;
  int f;
  int control;

  if(table->bucket_pos[f_pos]->num_ref > 1) //Existe mais de um ref pro mesmo bucket
  {
    printf(" mais de uma ref, expandindo bucket");

    //Cria uma lista auxiliar
    Lista* lista_aux = NULL;
    if((lista_aux = (Lista*) malloc(sizeof(Lista))) == NULL)
        return NULL;

    Bucket* bucket_aux = NULL;
    if((bucket_aux = (Bucket*) malloc(sizeof(Bucket))) == NULL)
        return NULL;

    bucket_aux = table->bucket_pos[f_pos];

    lista_aux = bucket_aux->pos;
    bucket_aux->num_ref--;
    table->bucket_pos[f_pos] = (Bucket*) criar_bucket(table);

    /*printf("lista_aux->tam = %d",lista_aux->tam);
    printf("\n imprime lista: ");
    lst_imprime(lista_aux);*/

    for(i=0;i<lista_aux->tam;i++)
    {

      f = get_bucket(table->bits,lst_getInfo(lista_aux,i));
      //printf("info = %d",lista_aux->info);
      if(f == f_pos)
      {
        //printf(" f = %d , mudando bucket",f);
        //printf("\n (antes1) Valor 10 = %d",table->bucket_pos[2]->pos->info);
        //printf("\n elemento = %d",lst_getInfo(lista_aux,i));
        table->bucket_pos[f_pos]->pos = lst_insere(table->bucket_pos[f_pos]->pos,lst_getInfo(lista_aux,i));
        //printf("\n (antes2) Valor 10 = %d",table->bucket_pos[2]->pos->info);
        table->bucket_pos[f_pos]->tam += 1;
        //printf("\n (antes3) Valor 10 = %d",table->bucket_pos[2]->pos->info[0]);

        control = lst_remove(lista_aux,lst_getInfo(lista_aux,i));

        /*if(control == 0)
          printf("\n nao foi possivel remover");
        else
          printf("\n removido com suc");*/

        /*printf("\n imprime lista: ");
        lst_imprime(lista_aux);*/

        bucket_aux->tam--;

        //printf("\n (depois) Valor 10 = %d",table->bucket_pos[2]->pos->info[0]);
      }
      else
      {
        //printf(" f = %d , mesmo bucket",f);
        //printf("\n Valor 10 = %d",table->bucket_pos[2]->pos->info[0]);
      }

    }

    //Se ap�s criar um novo bucket o bucket ja ficar cheio, faz processo novamente
    if(table->bucket_pos[f_pos]->tam == table->bucket_size)
    {
      printf("\n Foi criado um novo bucket ja cheio, nova realocacao\n");
      table = realloc_bucket(table,f_pos);
    }

    //printf("\n bucket_aux->tamanho = %d  e bucket_size = %d",bucket_aux->tam,table->bucket_size);
    //Se ap�s criar um novo bucket nenhum elemento for removido, faz processo novamente
    if(bucket_aux->tam == table->bucket_size)
    {
      printf("\n Foi criado um novo bucket vazio, nova realocacao\n");
      table = realloc_bucket(table,f);
    }


  }
  else //S� existe uma ref pro bucket, necess�rio aumentar num_bits
  {
    printf(" so uma ref, expandindo mascara de bits");
    table->bits++;
    size = pow(2,table->bits);

    if((table->bucket_pos = (Bucket**) realloc(table->bucket_pos,sizeof(Bucket*)*size)) == NULL)
    return NULL;

    for(i = pow(2,table->bits-1); i < size; i++)
    {
      //printf("\n id = %d",table->bucket_pos[get_bucket_byBit(table->bits-1,i)]->id);
      table->bucket_pos[i] = table->bucket_pos[get_bucket_byBit(table->bits-1,i)];
      table->bucket_pos[i]->num_ref++;
      //table->bucket_pos[i] = (Bucket*) criar_bucket(table);
    }

    //printf("\n Valor 10 = %d",table->bucket_pos[2]->pos->info[0]);

    realloc_bucket(table,f_pos);

    //printf("\nValor 00 = %d",table->bucket_pos[0]->pos->info);
    //printf("\nValor 01 = %d",table->bucket_pos[1]->pos->info);
    //printf("\n Valor 10 = %d",table->bucket_pos[2]->pos->info);
    //printf("\nValor 11 = %d",table->bucket_pos[3]->pos->info);

  }



  return table;
}



Hashtable* inserir_tabela(Hashtable* tabela, int info)
{
    int f = get_bucket(tabela->bits,info);

    //printf("f = %d",f);
    //printf("\n tamanho tab = %d e tabela->bucket_size=%d",tabela->bucket_pos[f]->tam,tabela->bucket_size);

    if(tabela->bucket_pos[f]->tam == tabela->bucket_size) // bucket com cap maxima, neces realocar
    {
      //tabela->bits += 1;
      tabela = realloc_bucket(tabela,f);
      f = get_bucket(tabela->bits,info);
      //continuar daqui, ta realocando bucket suficiente pra num de bits mas o certo e ir alocando de 1-1 conforme necessidade
    }


    tabela->bucket_pos[f]->pos = lst_insere(tabela->bucket_pos[f]->pos,info);
    tabela->bucket_pos[f]->tam += 1;



    if(tabela->bucket_pos[f]->pos != NULL)
      printf("\n Elemento %d inserido com sucesso",info);
    else
      printf("\n Nao foi possivel inserir o elemento");

    printf("\n Imprime bucket %d: ",tabela->bucket_pos[f]->id);
    lst_imprime(tabela->bucket_pos[f]->pos);


    return tabela;
}

int buscar_tabela(Hashtable* tabela, int info)
{
    int f = get_bucket(tabela->bits,info);

    int aux = lst_busca(tabela->bucket_pos[f]->pos,info);

    if(aux != -1)
      return tabela->bucket_pos[f]->pos->info[aux];

    return -1;
}

int remover_tabela(Hashtable* tabela, int info)
{
    int f = get_bucket(tabela->bits,info);

    Lista* lista = NULL;

    lista = tabela->bucket_pos[f]->pos;

    return lst_remove(lista,info);

}

void imprimir_todosBuckets(Hashtable* tabela)
{
  int i;

  Bucket* bucket = NULL;
  if((bucket = malloc(sizeof(Bucket))) == NULL)
    return NULL;

  printf("\n Tabela com mascara de %d bits",tabela->bits);
  printf("\n Possiblidade de armazenar %.0lf buckets",pow(2,tabela->bits));
  printf("\n Estao sendo utilizados %.d buckets\n",tabela->num_buckets);

  for(i=0;i<pow(2,tabela->bits);i++)
  {
    bucket = tabela->bucket_pos[i];
    printf("\n Imprime referencia %d no bucket %d: ",i+1,bucket->id);
    lst_imprime(bucket->pos);

  }
}

/*void liberar_tabela(Hashtable* table)
{
    int i;
    Lista* lst;
    for(i=0;i<table->tam;i++)
    {
        lst = table->pos[i];
        while(lst != NULL)
        {
            Lista* t = lst->prox;
            free(lst);
            lst = t;
        }
    }
}*/





