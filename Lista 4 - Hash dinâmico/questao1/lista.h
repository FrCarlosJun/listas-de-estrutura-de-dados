#ifndef LISTA_H_INCLUDED
#define LISTA_H_INCLUDED

#include <stdlib.h>
#include <stdio.h>

struct lista
{
  int* info;
  int tam;
};

typedef struct lista Lista;

/** Cria lista vazia*/
Lista* lst_cria (int tam);

/** Verifica se a lista ta vazia */
int lst_vazia(Lista* lst);

/** Insere novo elemento na cabe�a da lista */
Lista* lst_insere (Lista* lst, int val);

/** Imprime lista */
void lst_imprime (Lista* lst);

/** Busca um elemento na lista, retorna null caso n�o encontre*/
int lst_busca (Lista* lst, int val);

/** Remove elemento da lista*/
int lst_remove (Lista* lst, int val);

int lst_getInfo (Lista* lst, int pos);

/** Libera a lista completa */
void lst_libera (Lista* lst);


#endif // LISTA_H_INCLUDED
