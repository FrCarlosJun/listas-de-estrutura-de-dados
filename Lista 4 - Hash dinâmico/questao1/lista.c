#include "lista.h"


Lista* lst_cria(int tam)
{
  Lista* lista = (Lista*) malloc(sizeof(Lista));
  lista->info = (int*) calloc(tam,sizeof(int));
  lista->tam = tam;

  return lista;
}

int lst_getInfo(Lista* lst, int pos)
{
  return *(lst->info+pos);
}

int lst_vazia(Lista* lst)
{
  int i;
  for(i=0;i<lst->tam;i++)
  {
    if(*(lst->info+i)!=0)
      return 1;
  }
  return 0;

}

Lista* lst_insere (Lista* lst, int val)
{
  int i;
  //printf("\n tam = %d",lst->tam);
  for(i=0;i<lst->tam;i++)
  {
    //printf("\n val = %d",*(lst->info+i));
    if(*(lst->info+i)==0)
    {
      *(lst->info+i) = val;
      return lst;
    }
  }
  return NULL;
}

void lst_imprime(Lista* lst)
{
  int i;
  for(i=0;i<lst->tam;i++)
  {
    printf("\n pos[%d] = %d",i,*(lst->info+i));
  }
}

int lst_busca (Lista* lst, int val)
{
  int i;
  for(i=0;i<lst->tam;i++)
  {
    if(*(lst->info+i)==val)
    {
      return i;
    }
  }
  return -1;
}

int lst_remove (Lista* lst, int val)
{
  int i;
  for(i=0;i<lst->tam;i++)
  {
    if(*(lst->info+i)==val)
    {
      *(lst->info+i) = 0;
      return 1;
    }
  }
  return 0;

}

void lst_libera (Lista* lst)
{
    free(lst->info);
    free(lst);
}




