#include <stdio.h>
#include <stdlib.h>
#include "hashtable.h"

void iniciar()
{
  printf("\n ------------------------------------------- ");
  printf("\n --------- Iniciando a tabela Hash --------- ");
  printf("\n -------------------------------------------\n ");

}
void menu()
{
  printf("\n ------------------------------------------- ");
  printf("\n ------ Digite a opcao e aperte enter ------ ");
  printf("\n -------------------------------------------\n ");
  printf("\n 1. Inserir um elemento. ");
  printf("\n 2. Buscar um elemento. ");
  printf("\n 3. Remover um elemento. ");
  printf("\n 4. Imprime todas as referencias de buckets. ");
  printf("\n 5. Liberar tabela e sair. \n ");
}

int main()
{
  int tam;
  int opcao;
  int control;
  int value;
  Lista* lista;
  Hashtable* table;

  iniciar();
  printf("\n Digite o tamanho do bucket: ");
  scanf("%d",&tam);

  table = criar_tabela(tam);
  printf("\n Tabela hash com buckets de tamanho %d criada com sucesso!",tam);

  while(opcao != 5)
  {
    menu();

    scanf("%d",&opcao);

    switch(opcao)
    {
      case 1:
      {
        printf("\n Digite o numero inteiro que deseja inserir: ");
        scanf("%d",&value);
        table = inserir_tabela(table,value);
        break;
      }
      case 2:
      {
        printf("\n Digite o numero inteiro que deseja buscar: ");
        scanf("%d",&value);
        control = buscar_tabela(table,value);
        if(control == -1)
          printf("\nElemento %d nao encontrado!",value);
        else
          printf("\nElemento %d encontrado!",value);
        break;
      }
      case 3:
      {
        printf("\n Digite o numero inteiro que deseja remover: ");
        scanf("%d",&value);
        control = remover_tabela(table,value);
        if(control == 0)
          printf("\nElemento %d nao existe na tabela!",value);
        else
          printf("\nElemento %d removido com sucesso!",value);
        break;
      }
      case 4:
      {
        imprimir_todosBuckets(table);
        break;
      }
      default:
      {
        if(opcao == 5)
          break;
        else
          printf("\n opcao inexistente!\n");
      }
    }
  }

  //liberar_tabela(table);
  printf("\n Tabela liberada com sucesso! :)\n Finalizando aplicacao.\n");

  return 1;

}
