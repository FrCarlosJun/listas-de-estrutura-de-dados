#include <stdio.h>
#include <stdlib.h>
#include "florestaNo.h"

int main()
{
  int tam;
  int i;
  int elem1, elem2;

  FlorestaNo* no_aux;

  printf("\n DICA: Insira um valor maior que 5 para os testes");
  printf("\n Digite o tamanho do universo de valores n: ");
  scanf("%d",&tam);

  FlorestaNo* floresta[tam];
  for(i=0;i<tam;i++)
    floresta[i] = FLORESTANO_MakeSet(i+1);

  printf("\n Fazendo Union(1,2)");
  FLORESTANO_Union(floresta[0],floresta[1]);
  printf("\n Representante Find(1) = %d",(FLORESTANO_Find(floresta[0]))->valor);
  printf("\n Representante Find(2) = %d",(FLORESTANO_Find(floresta[1]))->valor);

  printf("\n\n Fazendo Union(3,4)");
  FLORESTANO_Union(floresta[2],floresta[3]);
  printf("\n Representante Find(3) = %d",(FLORESTANO_Find(floresta[2]))->valor);
  printf("\n Representante Find(4) = %d",(FLORESTANO_Find(floresta[3]))->valor);

  printf("\n\n Fazendo Union(1,3) -> Union([1,2],[3,4])");
  FLORESTANO_Union(floresta[0],floresta[2]);
  printf("\n Representante Find(1) = %d",(FLORESTANO_Find(floresta[0]))->valor);
  printf("\n Representante Find(2) = %d",(FLORESTANO_Find(floresta[1]))->valor);
  printf("\n Representante Find(3) = %d",(FLORESTANO_Find(floresta[2]))->valor);
  printf("\n Representante Find(4) = %d",(FLORESTANO_Find(floresta[3]))->valor);

  elem1 = 4;
  elem2 = 5;
  if(FLORESTANO_Equal(floresta[elem1-1],floresta[elem2-1]))
    printf("\n\n Os elementos %d e %d fazem parte do mesmo conjunto",elem1,elem2);
  else
    printf("\n\n Os elementos %d e %d nao fazem parte do mesmo conjunto",elem1,elem2);

  elem1 = 4;
  elem2 = 1;
  if(FLORESTANO_Equal(floresta[elem1-1],floresta[elem2-1]))
    printf("\n\n Os elementos %d e %d fazem parte do mesmo conjunto",elem1,elem2);
  else
    printf("\n\n Os elementos %d e %d nao fazem parte do mesmo conjunto",elem1,elem2);

  printf("\n\n Liberando a colecao");
  FLORESTANO_Free(floresta,tam);
  printf("\n Liberado com sucesso! :)\n");

  return 1;
}
