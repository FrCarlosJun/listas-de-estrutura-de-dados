#include "florestaNo.h"

FlorestaNo* FLORESTANO_MakeSet(int valor)
{
  FlorestaNo* no = (FlorestaNo*) malloc(sizeof(FlorestaNo));
  no->valor = valor;
  no->ranque = 0;
  no->pai = NULL;

  return no;
}

void FLORESTANO_Union(FlorestaNo* no1, FlorestaNo* no2)
{
  if(no2->ranque > no1->ranque)
  {
    no1->pai = no2;
    return;
  }
  else if(no1->ranque > no2->ranque)
  {
    no2->pai = no1;
  }
  else
  {
    no2->pai = no1;
    no1->ranque++;
  }
}

FlorestaNo* FLORESTANO_Find(FlorestaNo* no)
{
  FlorestaNo* temp;
  FlorestaNo* raiz = no;

  if(no->pai == NULL)
    return no;

  while(raiz->pai != NULL)
    raiz = raiz->pai;

  while(no->pai != raiz)
  {
    temp = no->pai;
    no->pai = raiz;
    no = temp;
  }

  return raiz;
}

int FLORESTANO_Equal(FlorestaNo* no1, FlorestaNo* no2)
{
  return ((FLORESTANO_Find(no1))->valor == (FLORESTANO_Find(no2))->valor);
}

void FLORESTANO_Free(FlorestaNo** floresta, int tam)
{
  int i;

  for(i=0;i<tam;i++)
  {
    free(floresta[i]->pai);
    free(floresta[i]);
  }
}
