#ifndef FLORESTANO_H_INCLUDED
#define FLORESTANO_H_INCLUDED

#include <stdio.h>

struct florestaNo
{
  int valor;
  int ranque;
  struct florestaNo* pai;
};

typedef struct florestaNo FlorestaNo;

/** Cria um conjunto com um s� elemento */
FlorestaNo* FLORESTANO_MakeSet(int valor);

/** Faz a uni�o entre dois conjuntos */
void FLORESTANO_Union(FlorestaNo* no1, FlorestaNo* no2);

/** Retorna o presentante  do conjunto*/
FlorestaNo* FLORESTANO_Find(FlorestaNo* no);

/** Verifica se dois n�s fazem parte do mesmo conjunto */
int FLORESTANO_Equal(FlorestaNo* no1, FlorestaNo* no2);

/** Libera toda a �rvore de conjuntos */
void FLORESTANO_Free(FlorestaNo** floresta, int tam);

#endif // FLORESTANO_H_INCLUDED
