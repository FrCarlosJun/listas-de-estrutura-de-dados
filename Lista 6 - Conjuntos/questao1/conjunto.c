#include "conjunto.h"

struct mapa
{
  int tamanho;
  char** dataset;
};

struct conjunto
{
  Mapa* mapa;
  int value;
};

Conjunto* CONJUNTO_CriarConjunto(Mapa* mapa)
{
  Conjunto* conjunto = (Conjunto*) malloc(sizeof(Conjunto));
  conjunto->mapa = mapa;
  if(mapa == NULL || mapa->tamanho == 0)
    return conjunto;

  if(mapa->tamanho > sizeof(int)*8)
  {
    printf("Mapa muito grande, o conjunto precisa ter no maximo 32 valores");
    return conjunto;
  }

  conjunto->value = 0b0;
  return conjunto;

}

static int mapa_getIndex(Mapa* mapa, char* str)
{
  int i;
  for(i=0;i<mapa->tamanho;i++)
  {
    if(strcmp(mapa->dataset[i],str) == 0)
      return i;

  }
  return -1;
}

Conjunto* CONJUNTO_InserirConjunto(Conjunto* conjunto, char* str)
{
  int index = mapa_getIndex(conjunto->mapa,str);
  if(index == -1)
  {
    printf("Elemento nao encontrado no mapa");
    return conjunto;
  }
  conjunto->value |=  (0b1 << index);
  return conjunto;
}

Conjunto* CONJUNTO_RemoverElemento(Conjunto* conjunto, char* str)
{
  int index = mapa_getIndex(conjunto->mapa,str);
  if(index == -1)
  {
    printf("Elemento nao encontrado no mapa");
    return conjunto;
  }
  conjunto->value &= ~(0b1 << index);
  return conjunto;
}

Conjunto* CONJUNTO_UnirConjunto(Conjunto* conjunto1, Conjunto* conjunto2)
{
  Conjunto* conjunto = (Conjunto*) malloc(sizeof(Conjunto));
  conjunto->mapa = conjunto1->mapa;
  conjunto->value = conjunto1->value | conjunto2->value;
  return conjunto;
}

Conjunto* CONJUNTO_IntersecConjunto(Conjunto* conjunto1, Conjunto* conjunto2)
{
  Conjunto* conjunto = (Conjunto*) malloc(sizeof(Conjunto));
  conjunto->mapa = conjunto1->mapa;
  conjunto->value = conjunto1->value & conjunto2->value;
  return conjunto;
}

Conjunto* conjunto_difer(Conjunto* conjunto1, Conjunto* conjunto2)
{
  Conjunto* conjunto = (Conjunto*) malloc(sizeof(Conjunto));
  conjunto->mapa = conjunto1->mapa;
  conjunto->value = conjunto1->value & ~conjunto2->value;
  return conjunto;
}

int CONJUNTO_VerificaSubConjunto(Conjunto* conjunto1, Conjunto* conjunto2)
{
  int value;
  value = conjunto1->value & ~conjunto2->value;
  if(value == 0)
    return 1;
  else
    return 0;
}

int conjunto_igual(Conjunto* conjunto1, Conjunto* conjunto2)
{
  int value;
  value = conjunto1->value ^ conjunto2->value;
  if(value == 0)
    return 1;
  else
    return 0;
}

void CONJUNTO_Complemento(Conjunto* conjunto)
{
  conjunto->value = ~conjunto->value;
}

int CONJUNTO_Pertence(Conjunto* conjunto, char* str)
{
  int value;
  int index = mapa_getIndex(conjunto->mapa,str);
  if(index == -1)
  {
    printf("Elemento nao encontrado no mapa");
    return conjunto;
  }
  value = conjunto->value & (0b1 << index);
  if(value == 0)
    return 0;
  else
    return 1;
}

static int countBits(size_t const size, void const * const ptr)
{
    unsigned char *b = (unsigned char*) ptr;
    unsigned char byte;
    int i, j;
    int count = 0;

    for (i=size-1;i>=0;i--)
    {
        for (j=7;j>=0;j--)
        {
            byte = (b[i] >> j) & 1;
            if(byte == 1)
              count++;
        }
    }

    return count;

}

int CONJUNTO_NumeroElemento(Conjunto* conjunto)
{
  return countBits(sizeof(conjunto->value),&(conjunto->value));
}

static void printBits(size_t const size, void const * const ptr)
{
    unsigned char *b = (unsigned char*) ptr;
    unsigned char byte;
    int i, j;

    for (i=size-1;i>=0;i--)
    {
        for (j=7;j>=0;j--)
        {
            byte = (b[i] >> j) & 1;
            printf("%u", byte);
        }
    }
    puts("");
}


void CONJUNTO_ImprimeConjunto(Conjunto* conjunto)
{
  printBits(sizeof(conjunto->value),&(conjunto->value));
}


Mapa* CONJUNTO_CriarMapa(int n)
{
  int i;
  Mapa* mapa = (Mapa*) malloc(sizeof(Mapa));
  mapa->tamanho = n;
  mapa->dataset = (char**) malloc(sizeof(char*)*n);
  for(i=0;i<n;i++)
  {
    mapa->dataset[i] = (char*) malloc(sizeof(char)*MAX_STR);
  }

  return mapa;

}

Mapa* CONJUNTO_PreencherMapa(Mapa* mapa)
{
  char* str_aux;
  int i;

  str_aux = (char*) malloc(sizeof(char)*MAX_STR*5);

  printf("\n DICA: \"um\" \"dois\" \"tres\" ...\n");
  for(i=0;i<mapa->tamanho;i++)
  {
    do
    {
      printf(" Digite o elemento %d do mapa: ",i+1);
      scanf("%s",str_aux);
    }while((strlen(str_aux) > MAX_STR) ? (printf(" String maior que tamanho permitido de %d caracteres\n",MAX_STR)) : (0));
    strcpy(mapa->dataset[i],str_aux);
  }

  return mapa;

}

void CONJUNTO_ImprimeMapa(Mapa* mapa)
{
  int i;

  for(i=0;i<mapa->tamanho;i++)
  {
    printf("\n Elemento %d: %s ",i+1,mapa->dataset[i]);
  }
}

void mapa_libera(Mapa* mapa)
{
  int i;
  if(mapa != NULL)
  {
    for(i=0;i<mapa->tamanho;i++)
    {
      free(mapa->dataset[i]);
    }
    free(mapa);
  }
}

void CONJUNTO_LiberaConjunto(Conjunto* conjunto)
{
  mapa_libera(conjunto->mapa);
  free(conjunto);
}


