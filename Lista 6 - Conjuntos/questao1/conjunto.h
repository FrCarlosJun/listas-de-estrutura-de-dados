#ifndef CONJUNTO_H_INCLUDED
#define CONJUNTO_H_INCLUDED

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define MAX_STR 100

typedef struct mapa Mapa;

typedef struct conjunto Conjunto;

/** Cria mapa usado em conjuntos*/
Mapa* CONJUNTO_CriarMapa(int n);

/** Ler as entradas do mapa para preencher*/
Mapa* CONJUNTO_PreencherMapa(Mapa* mapa);

/** Imprime o mapa */
void CONJUNTO_ImprimeMapa(Mapa* mapa);

/** Cria um conjunto vazio */
Conjunto* CONJUNTO_CriarConjunto(Mapa* mapa);

/** Insere string no conjunto */
Conjunto* CONJUNTO_InserirConjunto(Conjunto* conjunto, char* str);

/** Imprime o oconjunto em formato bin�rio */
void CONJUNTO_ImprimeConjunto(Conjunto* conjunto);

/** Remove elemento do conjunto */
Conjunto* CONJUNTO_RemoverElemento(Conjunto* conjunto, char* str);

/** Realiza opera��o de uni�o entre dois conjuntos */
Conjunto* CONJUNTO_UnirConjunto(Conjunto* conjunto1, Conjunto* conjunto2);

/** Realiza opera��o de interse��o entre dois conjuntos */
Conjunto* CONJUNTO_IntersecConjunto(Conjunto* conjunto1, Conjunto* conjunto2);

/** Verifica se conjunto B � subconjunto do conjunto A */
int CONJUNTO_VerificaSubConjunto(Conjunto* conjunto1, Conjunto* conjunto2);

/** Realiza opera��o de complemento em um conjunto */
void CONJUNTO_Complemento(Conjunto* conjunto);

/** Verifica se elemento pertence ao conjunto */
int CONJUNTO_Pertence(Conjunto* conjunto, char* str);

/** Retorna o numero de elementos do conjunto */
int CONJUNTO_NumeroElemento(Conjunto* conjunto);

/** Libera a mem�ria do conjunto */
void CONJUNTO_LiberaConjunto(Conjunto* conjunto);

#endif // CONJUNTO_H_INCLUDED
