#include <stdio.h>
#include <stdlib.h>
#include "conjunto.h"

int main()
{
  int n;
  int result;
  char* elemento = "um";

  Mapa* mapa;
  Conjunto* conjunto;
  Conjunto* conjunto2;
  Conjunto* conjunto3;
  Conjunto* conjunto4;

  do
  {
    printf("\n Digite o numero de elementos do conjunto: ");
    scanf("%d",&n);

    if(n > 32)
      printf("\n Mapa muito grande, o conjunto precisa ter no maximo 32 valores");
  }while(n > 32);


  mapa = CONJUNTO_CriarMapa(n);
  mapa = CONJUNTO_PreencherMapa(mapa);
  CONJUNTO_ImprimeMapa(mapa);

  conjunto = CONJUNTO_CriarConjunto(mapa);
  conjunto = CONJUNTO_InserirConjunto(conjunto,"um");
  conjunto = CONJUNTO_InserirConjunto(conjunto,"tres");
  conjunto = CONJUNTO_InserirConjunto(conjunto,"dois");
  printf("\n\n Imprime conjunto 1: ");
  CONJUNTO_ImprimeConjunto(conjunto);
  conjunto = CONJUNTO_RemoverElemento(conjunto,"um");
  printf("\n Imprime conjunto 1 apos remocao: ");
  CONJUNTO_ImprimeConjunto(conjunto);

  conjunto2 = CONJUNTO_CriarConjunto(mapa);
  conjunto2 = CONJUNTO_InserirConjunto(conjunto2,"um");
  printf("\n Imprime conjunto 2: ");
  CONJUNTO_ImprimeConjunto(conjunto2);
  conjunto3 = CONJUNTO_UnirConjunto(conjunto,conjunto2);
  printf("\n Imprime conjunto 3 que eh uniao de 1 e 2: ");
  CONJUNTO_ImprimeConjunto(conjunto3);

  conjunto4 = CONJUNTO_IntersecConjunto(conjunto3,conjunto2);
  printf("\n Imprime conjunto apos intersecao entre conj 3 e 2: ");
  CONJUNTO_ImprimeConjunto(conjunto4);

  conjunto4 = conjunto_difer(conjunto3,conjunto2);
  printf("\n Imprime conjunto apos diferenca entre 3 e 2: ");
  CONJUNTO_ImprimeConjunto(conjunto4);

  printf("\n Testa se conjunto 2 eh subconj de 1: ");
  if(CONJUNTO_VerificaSubConjunto(conjunto2,conjunto) == 1)
    printf("Sim eh subconj");
  else
    printf("nao eh subconj");

  printf("\n Testa se conjunto 1 e 2 sao iguais: ");
  if(conjunto_igual(conjunto2,conjunto) == 1)
    printf("Sao iguais");
  else
    printf("Nao sao iguais");

  CONJUNTO_Complemento(conjunto);
  printf("\n Imprime o conjunto 1 apos complemento: ");
  CONJUNTO_ImprimeConjunto(conjunto);

  printf("\n Testa se elemento \"%s\" pertence conjunto 2: ",elemento);
  if(CONJUNTO_Pertence(conjunto2,elemento) == 1)
    printf("Pertence");
  else
    printf("Nao Pertence");

  printf("\n num elementos  do conjunto 2 = %d",CONJUNTO_NumeroElemento(conjunto2));

  CONJUNTO_LiberaConjunto(conjunto);
  printf("\n Lista liberada! fim :)\n");

  return 0;
}
