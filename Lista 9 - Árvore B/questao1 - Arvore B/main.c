#include <stdio.h>
#include <stdlib.h>
#include "btree.h"


int main()
{
  int order;
  BTree* bt = NULL;
  BTNode* btNode = NULL;
  int chave;

  order = 5;
  bt = BTREE_create_empty(order);

  BTREE_insert(bt,10,100.0);
  BTREE_insert(bt,20,200.0);
  BTREE_insert(bt,30,300.0);
  BTREE_insert(bt,40,400.0);
  BTREE_insert(bt,50,500.0);
  BTREE_insert(bt,60,600.0);

  BTREE_insert(bt,25,250.0);
  BTREE_insert(bt,80,800.0);
  BTREE_insert(bt,5,50.0);
  BTREE_insert(bt,7,70.0);

  BTREE_insert(bt,90,800.0);
  BTREE_insert(bt,100,1000.0);
  BTREE_insert(bt,110,1100.0);
  BTREE_insert(bt,120,1200.0);
  BTREE_insert(bt,130,1300.0);
  BTREE_insert(bt,140,1300.0);
  BTREE_insert(bt,150,1500.0);

  BTREE_insert(bt,160,1600.0);
  BTREE_insert(bt,170,1700.0);
  BTREE_insert(bt,180,1800.0);

  chave = 5;
  printf("\n Procurando o elemento de chave %d na arvore",chave);
  btNode = BTREE_search(bt->root,chave);
  if(btNode != NULL)
    printf("\n Elemento de chave %d encontrado, valor = %f \n",chave,btNode->info);
  else
    printf("\n Elemento de chave %d nao encontrado \n",chave);

  printf("\n Imprimindo toda a arvore\n");
  BTREE_print_tree(bt->root);

  return 1;

}
