#include "btree.h"

static BTPage* BTREE_page_create_empty(BTree* bt, BTPage* pgParent)
{
  int i;

  // Aloca memoria para estrutura de pagina
  BTPage* pgCurrent = (BTPage*) malloc(sizeof(BTPage));
  //Inicializa pagina vazia com zero elementos
  pgCurrent->numkeys = 0;
  //Aponta para a pagina pai
  pgCurrent->parent = pgParent;

  //Aloca memoria para os nos (key + info) e filhos
  //(s�o alocados order nos, ou seja um amais, para armazenar overflow tempor�rios
  pgCurrent->node = (BTNode**) malloc((bt->order)*sizeof(BTNode*));
  pgCurrent->rchild = (BTPage**) malloc((bt->order)*sizeof(BTPage*));

  //Inicializa os nos e os ponteiros para filhos com null
  pgCurrent->lchild = NULL;
  for(i=0;i<bt->order;i++)
  {
    pgCurrent->node[i] = NULL;
    pgCurrent->rchild[i] = NULL;
  }

  return pgCurrent;

}

BTree* BTREE_create_empty(int order)
{
  BTree* bt = (BTree*) malloc(sizeof(BTree));
  bt->order = order;
  bt->root = BTREE_page_create_empty(bt,NULL);

  return bt;
}

BTNode* BTREE_node_Create(int key, float info)
{
  BTNode* btNode = (BTNode*)malloc(sizeof(BTNode));

  btNode->key = key;
  btNode->info = info;

  return btNode;

}

static int BTREE_page_overflowed(BTree* bt,BTPage* pgCurrent)
{
  return (pgCurrent->numkeys == bt->order);
}


int BTREE_page_find_insertion_point(BTPage* pgParent, BTNode* nodeMedian)
{
  int i = 0;
  while(pgParent->node[i] != NULL)
  {
    if(pgParent->node[i]->key < nodeMedian->key)
      i++;
    else
      break;
  }

  return i;
}


static void BTREE_page_insert_node_at(BTPage* pgCurrent,BTNode* btNode, int position, int order)
{
  int i;

  // Se ja existir um elemento na posi��o a ser inserida
  // os elementos posteriores s�o empurrados at� liberar o espa�o
  if(pgCurrent->node[position] != NULL)
  {
    // Navega pelos n�s da page de tr�s para frente
    for(i = order-1; i >= 0; i--)
    {
      // Se um n� for diferente de NULL empurra ele para frente
      if(pgCurrent->node[i] != NULL)
      {
        pgCurrent->rchild[i+1] = pgCurrent->rchild[i];
        pgCurrent->node[i+1] = pgCurrent->node[i];
      }
      //Se chegar na posi��o de inser��o, interrompe o processo
      // de empurrar
      if(i == position)
        break;
    }
  }

  // Se a posi��o para inserir o n� for NULL ou o elemento
  // foi puxado para frente, insere o n� na posi��o
  pgCurrent->node[position] = btNode;
  pgCurrent->numkeys++;

}

static BTPage* BTREE_page_split(BTree* bt,BTPage* pgCurrent)
{
  BTPage* pgParent = pgCurrent->parent;
  // Cria a pagina nova que ser� o rchild do split
  BTPage* pgNew = BTREE_page_create_empty(bt, pgParent);

  int i,j;

  // Calcula o indice do elemento medio para a divis�o de acordo com order
  int idxMedian = (int) (bt->order+1)/2 - 1;
  // A nova pagina � a pag direita (maior) do split, recebe como primeiro indice
  // o elemento medio d� pagina com overflow
  BTNode* nodeMedian = pgCurrent->node[idxMedian];
  // O lchild da nova pagina recebe o ponteiro do elemento medio
  pgNew->lchild = pgCurrent->rchild[idxMedian];
  // O pai da nova pagina � o pai da pagina antiga, pois a pagina antiga
  // que � o pgCurrent ser� a pagina esquerda do split
  pgNew->parent = pgParent;

  // Se o lchild da nova pagina direita n�o for NULL, que � sempre verdade
  // O pai da pagina apontado pelo lchild aponta para a pagina nova
  if(pgNew->lchild)
    pgNew->lchild->parent = pgNew;

  // Se o pai da pagina de entrada for igual a NULL, ent�o � porque o split
  // ocorre na raiz, ent�o � necess�rio criar um novo pai, que ser� a nova raiz
  if(pgParent == NULL)
  {
    pgParent = BTREE_page_create_empty(bt,NULL);
    // Com a nova pagina raiz cria os caminhos para direita e esquerda
    pgParent->lchild = pgCurrent;
    pgParent->rchild[0] = pgNew;
    pgCurrent->parent = pgParent;
    pgNew->parent = pgParent;
    bt->root = pgParent;
  }

  // Insere o ponto medio no pai
  BTREE_page_insert_node_at(pgParent, nodeMedian, BTREE_page_find_insertion_point(pgParent, nodeMedian),bt->order);

  // Se o pai da pagina de entrada for diferente de NULL, ent�o � porque o split
  // n�o ocorre na raiz, ent�o basta adicionar o ponteiro da pagina direita no pai
  if(pgParent != NULL)
  {
    pgParent->rchild[BTREE_page_find_insertion_point(pgParent, nodeMedian)] = pgNew;
  }

  //Mover os n�s que est�o a direita do n� medio para nova pagina
  for(j=0, i=idxMedian +1; i<pgCurrent->numkeys;++i,++j)
  {
    pgNew->node[j] = pgCurrent->node[i];
    pgNew->rchild[j] = pgCurrent->rchild[i];
    // Se a refer�ncia da pagina que foi transferida n�o for NULL,
    // Seta o pai dela para a pagina nova
    if(pgNew->rchild[j])
      pgNew->rchild[j]->parent = pgNew;
    pgNew->numkeys++;
    pgCurrent->node[i] = NULL;
    pgCurrent->rchild[i] = NULL;
  }

  // Remove o n� medio da pagina esquerda que ficou sobrando
  pgCurrent->node[idxMedian] = NULL;
  pgCurrent->rchild[idxMedian] = NULL;
  pgCurrent->numkeys = idxMedian;

  return pgParent;
}


static BTPage* BTREE_insert_aux(BTree* bt, BTPage* pgCurrent, BTNode* btNode)
{
  int i;
  if(pgCurrent == NULL)
  {
    BTPage* pgNew = BTREE_page_create_empty(bt,NULL);
    return pgNew;
  }
  //printf("\n num_keys = %d",pgCurrent->numkeys);
  // Procura onde a informa��o deve ser inserida
  for(i=0; i<pgCurrent->numkeys; ++i)
  {
    //Se a chave ja existe, apenas atualiza a informacao
    if(btNode->key == pgCurrent->node[i]->key)
    {
      pgCurrent->node[i]->info = btNode->info;
      return pgCurrent;
    }
    // Se a chave do no a ser inserido for menor que a chave procurada
    // entao ja sabe onde descer na arvore (i - esquerda)
    else if(btNode->key < pgCurrent->node[i]->key)
      break;
  }
  // Se o lchild for NULL, ent�o a pagina � folha
  if(pgCurrent->lchild == NULL)
  {
    //printf("\n eh folha");
    BTREE_page_insert_node_at(pgCurrent,btNode,i,bt->order);
  }
  // Se a pagina n�o for folha, e o indice for igual ou menor que
  // o primeiro indice da pagina, ent�o navega por lchild (recursao)
  else if(i == 0)
  {
    //printf("\n nao eh folha");
    BTREE_insert_aux(bt,pgCurrent->lchild,btNode);
  }
  // Se a pagina n�o for folha, e o indice for igual ou menor que
  // um outro indice da pagina, ent�o navega pelo rchild i-1 (recursao)
  else
  {
    //printf("\n navega");
    BTREE_insert_aux(bt,pgCurrent->rchild[i-1],btNode);
  }

  // Se a adi��o do novo n� causar um overflow, ent�o ocorre a separa��o
  if(BTREE_page_overflowed(bt,pgCurrent))
    pgCurrent = BTREE_page_split(bt,pgCurrent);

  return pgCurrent;

}

void BTREE_insert(BTree* bt, int key, float info)
{
  BTNode* btNode = BTREE_node_Create(key,info);
  BTPage* pgCurrent = BTREE_insert_aux(bt,bt->root,btNode);
  if(bt->root != pgCurrent)
  {
    bt->root = pgCurrent;
  }
}

BTNode* BTREE_search(BTPage* pgCurrent,int key)
{
  int i;

  if(pgCurrent == NULL)
    return NULL;

  for(i=0; i<pgCurrent->numkeys; ++i)
  {
    //Se encontrar chave retorna o no
    if(key == pgCurrent->node[i]->key)
    {
      return pgCurrent->node[i];
    }
    // Se a chave for menor que a chave procurada
    // entao ja sabe onde descer na arvore (i - esquerda)
    else if(key < pgCurrent->node[i]->key)
    {
      if(i==0)
        return BTREE_search(pgCurrent->lchild,key);
      else
        return BTREE_search(pgCurrent->rchild[i],key);
    }
  }
  return NULL;
}

void BTREE_print_tree(BTPage* pgCurrent)
{
  int i;

  for (i = 0; i < pgCurrent->numkeys; i++)
  {
    printf(" |%d|",pgCurrent->node[i]->key);
  }
  if(pgCurrent->lchild != NULL)
  {
    //printf("num_ keys = %d",pgCurrent->numkeys);
    printf("\n Pagina a esquerda de |%d| -> ",pgCurrent->node[0]->key);
    BTREE_print_tree(pgCurrent->lchild);
    for (i = 0; i < pgCurrent->numkeys; i++)
    {
      printf("\n Pagina a direita de |%d| -> ",pgCurrent->node[i]->key);
      BTREE_print_tree(pgCurrent->rchild[i]);
    }
    printf("\n");
  }
  //BTPage* pg_aux = bt->root;

}
