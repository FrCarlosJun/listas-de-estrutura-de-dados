#ifndef BTREE_H_INCLUDED
#define BTREE_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>

struct btreeNode
{
  int key;
  float info;
};

struct btreePage
{
  struct btreePage* parent;
  int numkeys;
  struct btreePage* lchild;
  struct btreePage** rchild;
  struct btreeNode** node;
};

struct btree
{
  int order;
  struct btreePage* root;
};

typedef struct btreeNode BTNode;

typedef struct btreePage BTPage;

typedef struct btree BTree;

/** Cria uma arvore b vazia */
BTree* BTREE_create_empty(int order);

/** Insere uma informa��o na �rvore */
void BTREE_insert(BTree* bt, int key, float info);

/** Imprime toda a arvore */
void BTREE_print_tree(BTPage* pgCurrent);

/** Procura pela chave e retorna o n� */
BTNode* BTREE_search(BTPage* pgCurrent,int key);

#endif // BTREE_H_INCLUDED
