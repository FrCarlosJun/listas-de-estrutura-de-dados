#ifndef MINHEAP_H_INCLUDED
#define MINHEAP_H_INCLUDED

#include <stdio.h>

#define SUCCESS 1
#define FAILED 0

typedef struct minheap MinHeap;

MinHeap* heap_cria(int qtd);

void heap_print(MinHeap* heap);

int heap_insere(MinHeap* heap, float value);

int heap_busca(MinHeap* heap, float value, int pai);

int heap_remove_elemento(MinHeap* heap, float value);

int heap_modifica_elemento(MinHeap* heap, float value, float new_value);

void heap_liberar(MinHeap* heap);

#endif // MINHEAP_H_INCLUDED
