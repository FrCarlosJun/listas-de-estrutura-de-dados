#include <stdio.h>
#include <stdlib.h>
#include "minheap.h"

int main()
{

  int n;
  int i;
  float e[10] = {4.0,2.0,9.0,5.0,13.0,7.0,6.0,3.0,1.0,15.0};
  float aux;
  float aux2;
  int control;

  MinHeap* heap;

  printf(" Digite o tamanho n do Min Heap: ");
  scanf("%d",&n);

  heap = heap_cria(n);
  if(heap != NULL)
    printf("\n Min Heap criado com sucesso! \n");

  for(i=0; i<10;i++)
  {
    if(heap_insere(heap,e[i]) == SUCCESS)
      printf("\n Elemento %f inserido com sucesso",e[i]);
    else
      printf("\n Nao foi possivel inserir o Elemento %f",e[i]);
  }

  printf("\n\n Visualizando a arvore ");
  heap_print(heap);

  printf("\n\n Digite o elemento que deseja buscar no Min Heap: ");
  scanf("%f",&aux);
  control = heap_busca(heap,aux,0);
  if(control != -1)
    printf(" Elemento encontrado na posicao %d!",control);
  else
    printf(" Elemento nao encontrado");

  printf("\n\n Digite o elemento que deseja remover no Min Heap: ");
  scanf("%f",&aux);
  control = heap_remove_elemento(heap,aux);
  if(control != -1)
    printf(" Elemento %f removido com sucesso",aux);
  else
    printf(" Elemento nao encontrado");

  printf("\n\n Visualizando a arvore ");
  heap_print(heap);

  printf("\n\n Digite o elemento que deseja trocar no Min Heap: ");
  scanf("%f",&aux);
  printf(" Digite o novo valor do elemento: ");
  scanf("%f",&aux2);
  control = heap_modifica_elemento(heap,aux,aux2);
  if(control != -1)
    printf(" Elemento trocado com sucesso ");
  else
    printf(" Elemento nao encontrado");

  printf("\n\n Visualizando a arvore ");
  heap_print(heap);

  heap_liberar(heap);
  printf("\n\n Fim! Heap liberado com sucesso! :)\n");


  return 0;
}
