#include "minheap.h"

struct minheap {
  int max;
  int pos;
  float *data;
};

static void troca(int a, int b, float* data)
{
  float aux;

  aux = data[a];
  data[a] = data[b];
  data[b] = aux;
}

int heap_busca(MinHeap* heap, float value, int pai)
{
  int pos = -1;

  if(pai >= heap->pos || heap->data[pai] > value)
    return pos;

  if(heap->data[pai] == value)
    return pai;
  else
  {
    if(2*pai+1 < heap->pos)
      pos = heap_busca(heap,value,2*pai+1);
    if(2*pai+2 < heap->pos && pos == -1)
      pos = heap_busca(heap,value,2*pai+2);
  }

  return pos;

}


void heap_print(MinHeap* heap)
{
  int i;
  printf("\n");
  for(i=0;i<heap->pos;i++)
    printf(" %f ",heap->data[i]);
}



static void corrige_acima(MinHeap* heap, int pos)
{
  while(pos > 0)
  {
    int pai = (pos-1)/2;
    if(heap->data[pai] > heap->data[pos])
      troca(pos,pai,heap->data);
    else
      break;
    pos=pai;
  }
}

MinHeap* heap_cria(int qtd)
{
  MinHeap* heap = (MinHeap*) malloc(sizeof(MinHeap));
  if(heap == NULL)
    return NULL;

  heap->data = (float*) malloc(sizeof(float)*qtd);
  if(heap->data == NULL)
    return NULL;

  heap->pos = 0;
  heap->max = qtd;

  return heap;
}

int heap_insere(MinHeap* heap, float value)
{
  if(heap->pos <= heap->max-1)
  {
    heap->data[heap->pos] = value;
    corrige_acima(heap,heap->pos);
    heap->pos++;

    return SUCCESS;
  }
  else
    return FAILED;
}

static void corrige_abaixo(MinHeap* heap, int pai)
{

  while(2*pai+1 < heap->pos)
  {
    int filho_esq = 2*pai+1;
    int filho_dir = 2*pai+2;
    int filho;

    if(filho_dir >= heap->pos)
      filho_dir = filho_esq;

    if(heap->data[filho_esq] > heap->data[filho_dir])
      filho = filho_dir;
    else
      filho = filho_esq;

    if(heap->data[pai]>heap->data[filho])
      troca(pai,filho,heap->data);
    else
      break;
    pai=filho;
  }
}


int heap_remove_elemento(MinHeap* heap, float value)
{
   int pos = heap_busca(heap,value,0);
   if(pos != -1)
   {
     heap->data[pos] = heap->data[heap->pos-1];
     heap->pos--;
     corrige_abaixo(heap,pos);
     corrige_acima(heap,pos);

     return 1;
   }

   return pos;
}

int heap_modifica_elemento(MinHeap* heap, float value, float new_value)
{
   int pos = heap_busca(heap,value,0);
   if(pos != -1)
   {
     heap->data[pos] = new_value;
     corrige_abaixo(heap,pos);
     corrige_acima(heap,pos);

     return 1;
   }

   return pos;
}

float heap_remove(MinHeap* heap)
{
  if(heap->pos>0)
  {
    float aux;
    aux = heap->data[0];
    heap->data[0] = heap->data[heap->pos-1];
    heap->pos--;
    corrige_abaixo(heap,0);
    return aux;
  }
  else
    return -1;
}

void heap_liberar(MinHeap* heap)
{
  free(heap->data);
  free(heap);
}
