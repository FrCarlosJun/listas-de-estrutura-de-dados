#include <stdio.h>
#include <stdlib.h>
#include "hashtable.h"

void iniciar()
{
  printf("\n ------------------------------------------- ");
  printf("\n --------- Iniciando a tabela Hash --------- ");
  printf("\n -------------------------------------------\n ");

}
void menu()
{
  printf("\n ------------------------------------------- ");
  printf("\n ------ Digite a opcao e aperte enter ------ ");
  printf("\n -------------------------------------------\n ");
  printf("\n 1. Inserir um elemento. ");
  printf("\n 2. Buscar um elemento. ");
  printf("\n 3. Remover um elemento. ");
  printf("\n 4. Liberar tabela e sair. \n ");
}

int main()
{
  int tam;
  int opcao;
  int control;
  int value;
  Lista* lista;
  Hashtable* table;

  iniciar();
  printf("\n Digite o numero de chaves n: ");
  scanf("%d",&tam);

  table = criar_tabela(tam/2);
  printf("\n Tabela hash com %d posicoes criada com sucesso!",tam/2);

  while(opcao != 4)
  {
    menu();

    scanf("%d",&opcao);

    switch(opcao)
    {
      case 1:
      {
        printf("\n Digite o numero inteiro que deseja inserir: ");
        scanf("%d",&value);
        table = inserir_tabela(table,value);
        if(table != NULL)
          printf("\n Elemento %d inserido com sucesso",value);
        else
          printf("\n nao foi possivel inserir o elemento");
        break;
      }
      case 2:
      {
        printf("\n Digite o numero inteiro que deseja buscar: ");
        scanf("%d",&value);
        lista = buscar_tabela(table,value);
        if(lista == NULL)
          printf("\nElemento %d nao encontrado!",value);
        else
          printf("\nElemento %d encontrado!",value);
        break;
      }
      case 3:
      {
        printf("\n Digite o numero inteiro que deseja remover: ");
        scanf("%d",&value);
        control = remover_tabela(table,value);
        if(control == 0)
          printf("\nElemento %d nao existe na tabela!",value);
        else
          printf("\nElemento %d removido com sucesso!",value);
        break;
      }
      default:
      {
        if(opcao == 4)
          break;
        else
          printf("\n opcao inexistente!\n");
      }
    }
  }

  liberar_tabela(table);
  printf("\n Tabela liberada com sucesso! :)\n Finalizando aplicacao.\n");

  return 1;

}
