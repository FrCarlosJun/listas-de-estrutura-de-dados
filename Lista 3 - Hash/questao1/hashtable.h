#ifndef HASHTABLE_H_INCLUDED
#define HASHTABLE_H_INCLUDED

#include "lista.h"
#include <stdlib.h>
#include <stdio.h>

struct hashtable
{
  int tam;
  struct Lista** pos;
};

typedef struct hashtable Hashtable;

/** Cria uma tabela  Has de inteiros do tamanho desejado*/
Hashtable* criar_tabela(int tam);

/** Insere um elemento inteiro na tabela*/
Hashtable* inserir_tabela(Hashtable* tabela, int info);

/** Busca o elemento na tabela e retorna a lista do elemento*/
Lista* buscar_tabela(Hashtable* tabela, int info);

/** Libera toda a tabela*/
void liberar_tabela(Hashtable* table);

#endif // HASHTABLE_H_INCLUDED
