#include "hashtable.h"
#include "lista.h"

Hashtable* criar_tabela(int tam)
{
    int i;
    Hashtable* table = NULL;
    if((table = malloc(sizeof(Hashtable))) == NULL)
        return NULL;

    table->tam = tam;
    if((table->pos = (Lista**) malloc(sizeof(Lista*)*tam)) == NULL)
        return NULL;

    for( i = 0; i < tam; i++ ) {
		table->pos[i] = NULL;
	}

    return table;
}

Hashtable* inserir_tabela(Hashtable* tabela, int info)
{
    int f;

    f = info%tabela->tam;

    Lista* lista = NULL;
    if((lista = malloc(sizeof(Lista))) == NULL)
        return NULL;

    lista->info = info;
    lista->prox = tabela->pos[f];
    tabela->pos[f] = lista;

    return tabela;
}

Lista* buscar_tabela(Hashtable* tabela, int info)
{
    int f;

    f = info%tabela->tam;

    Lista* lista = NULL;
    if((lista = malloc(sizeof(Lista))) == NULL)
        return NULL;

    lista = tabela->pos[f];

    while(lista != NULL)
    {
        if(lista->info == info)
          return lista;
        lista = lista->prox;
    }

    return lista;
}

int remover_tabela(Hashtable* tabela, int info)
{
    int f;

    f = info%tabela->tam;

    Lista* lista = NULL;
    Lista* ant = NULL;

    lista = tabela->pos[f];

    while(lista != NULL && lista->info != info)
    {
        ant = lista;
        lista = lista->prox;
    }

    if(lista == NULL)
        return 0;
    if(ant == NULL)
        tabela->pos[f] = lista->prox;
    else
        ant->prox = lista->prox;

    free(lista);
    return 1;

}

void liberar_tabela(Hashtable* table)
{
    int i;
    Lista* lst;
    for(i=0;i<table->tam;i++)
    {
        lst = table->pos[i];
        while(lst != NULL)
        {
            Lista* t = lst->prox;
            free(lst);
            lst = t;
        }
    }
}


