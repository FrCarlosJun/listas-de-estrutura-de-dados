#include "arvorebinaria.h"

ArvoreBinaria* ARVOREBINARIA_CriarArvoreVazia()
{
  return NULL;
}

static ArvoreBinaria* ARVOREBINARIA_NovoElemento(int elemento)
{
  ArvoreBinaria* arvore_nova = NULL;
  arvore_nova = (ArvoreBinaria*) malloc(sizeof(ArvoreBinaria));
  arvore_nova->valor = elemento;
  arvore_nova->pai = NULL;
  arvore_nova->dir = NULL;
  arvore_nova->esq = NULL;

  return arvore_nova;

}

ArvoreBinaria* ARVOREBINARIA_InserirElemento(ArvoreBinaria* arvore, int elemento)
{
  ArvoreBinaria* arvore_aux = arvore;

  if(arvore == NULL)
  {
    return ARVOREBINARIA_NovoElemento(elemento);
  }

  while(arvore_aux->valor != elemento)
  {
    if(elemento > arvore_aux->valor)
    {
      if(arvore_aux->dir != NULL)
        arvore_aux = arvore_aux->dir;
      else
      {
        arvore_aux->dir = ARVOREBINARIA_NovoElemento(elemento);
        arvore_aux->dir->pai = arvore_aux;
        return arvore;
      }
    }

    if(elemento < arvore_aux->valor)
    {
      if(arvore_aux->esq != NULL)
        arvore_aux = arvore_aux->esq;
      else
      {
        arvore_aux->esq = ARVOREBINARIA_NovoElemento(elemento);
        arvore_aux->esq->pai = arvore_aux;
        return arvore;
      }
    }
  }

  return arvore;
}

ArvoreBinaria* ARVOREBINARIA_BuscarElemento(ArvoreBinaria* arvore, int elemento)
{
  while(arvore != NULL)
  {
    if(arvore->valor == elemento)
      return arvore;

    if(elemento > arvore->valor)
      arvore = arvore->dir;
    else
      arvore = arvore->esq;
  }

  return arvore;

}

ArvoreBinaria* ARVOREBINARIA_MenorElemento(ArvoreBinaria* arvore)
{
  if(arvore == NULL)
    return arvore;

  while(arvore->esq != NULL)
    arvore = arvore->esq;

  return arvore;
}

ArvoreBinaria* ARVOREBINARIA_Sucessor(ArvoreBinaria* arvore)
{
  if(arvore == NULL)
    return arvore;

  if(arvore->dir != NULL)
    return ARVOREBINARIA_MenorElemento(arvore->dir);
  else
  {
    ArvoreBinaria* pai = arvore->pai;
    while(pai != NULL && pai->dir == arvore)
    {
      arvore = pai;
      pai = pai->pai;
    }

    return pai;
  }

}

ArvoreBinaria* ARVOREBINARIA_Remover(ArvoreBinaria* arvore, int elemento)
{
  if(arvore == NULL)
    return NULL;
  else if(elemento < arvore->valor)
    ARVOREBINARIA_Remover(arvore->esq,elemento);
  else if(elemento < arvore->valor)
    ARVOREBINARIA_Remover(arvore->dir,elemento);
  else
  {
    //No sem nenhum filho
    if(arvore->dir == NULL && arvore->esq == NULL)
    {
      free(arvore);
      arvore = NULL;
    }
    else if(arvore->esq == NULL)
    {
      ArvoreBinaria* arvore_aux = arvore;
      arvore = arvore->dir;
      arvore->pai = arvore_aux->pai;
      free(arvore_aux);
      arvore_aux = NULL;
    }
    else if(arvore->dir == NULL)
    {
      ArvoreBinaria* arvore_aux = arvore;
      arvore = arvore->esq;
      arvore->pai = arvore_aux->pai;
      free(arvore_aux);
      arvore_aux = NULL;
    }
    else
    {
      ArvoreBinaria* arvore_sucessor = ARVOREBINARIA_Sucessor(arvore);
      arvore->valor = arvore_sucessor->valor;
      arvore_sucessor->valor = elemento;
      if(arvore_sucessor->pai->esq == arvore_sucessor)
        arvore_sucessor->pai->esq = arvore_sucessor->dir;
      else
        arvore_sucessor->pai->dir = arvore_sucessor->dir;

      if(arvore_sucessor->dir != NULL)
      {
        arvore_sucessor->dir->pai = arvore_sucessor->pai;
        free(arvore_sucessor);
        arvore_sucessor = NULL;
      }
    }
  }

  return arvore;
}

void ARVOREBINARIA_Imprimir_PreOrdem(ArvoreBinaria* arvore)
{
  if(arvore == NULL)
    return;
  printf("%d ",arvore->valor);
  ARVOREBINARIA_Imprimir_PreOrdem(arvore->esq);
  ARVOREBINARIA_Imprimir_PreOrdem(arvore->dir);
}

void ARVOREBINARIA_Imprimir_PosOrdem(ArvoreBinaria* arvore)
{
  if(arvore == NULL)
    return;
  ARVOREBINARIA_Imprimir_PosOrdem(arvore->esq);
  ARVOREBINARIA_Imprimir_PosOrdem(arvore->dir);
  printf("%d ",arvore->valor);
}

void ARVOREBINARIA_Imprimir_EmOrdem(ArvoreBinaria* arvore)
{
  if(arvore == NULL)
    return;
  ARVOREBINARIA_Imprimir_EmOrdem(arvore->dir);
  printf("%d ",arvore->valor);
  ARVOREBINARIA_Imprimir_EmOrdem(arvore->esq);
}
