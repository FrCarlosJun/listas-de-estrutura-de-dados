#include <stdio.h>
#include <stdlib.h>
#include "arvorebinaria.h"

int main()
{
    ArvoreBinaria* arvore;
    ArvoreBinaria* arvore_aux;
    int elemento;

    arvore = ARVOREBINARIA_CriarArvoreVazia();

    printf("\n Inserindo arvore do slide 43");
    arvore = ARVOREBINARIA_InserirElemento(arvore,7);
    arvore = ARVOREBINARIA_InserirElemento(arvore,4);
    arvore = ARVOREBINARIA_InserirElemento(arvore,9);
    arvore = ARVOREBINARIA_InserirElemento(arvore,2);
    arvore = ARVOREBINARIA_InserirElemento(arvore,5);
    arvore = ARVOREBINARIA_InserirElemento(arvore,6);
    arvore = ARVOREBINARIA_InserirElemento(arvore,1);
    arvore = ARVOREBINARIA_InserirElemento(arvore,8);
    arvore = ARVOREBINARIA_InserirElemento(arvore,3);
    arvore = ARVOREBINARIA_InserirElemento(arvore,10);

    elemento = 8;
    printf("\n\n Procurando elemento %d",elemento);
    arvore_aux = ARVOREBINARIA_BuscarElemento(arvore,elemento);
    if(arvore_aux == NULL)
      printf("\n Elemento %d nao encontrado!",elemento);
    else
      printf("\n Elemento %d encontrado!",elemento);

    printf("\n\n Procurando sucessor do elemento %d",elemento);
    arvore_aux = ARVOREBINARIA_Sucessor(arvore_aux);
    if(arvore_aux == NULL)
      printf("\n nao possui sucessor");
    else
      printf("\n elemento sucessor = %d",arvore_aux->valor);

    elemento = 4;
    printf("\n\n Removendo elemento %d",elemento);
    arvore = ARVOREBINARIA_Remover(arvore,elemento);

    printf("\n\n Imprimir em pre-ordem: ");
    ARVOREBINARIA_Imprimir_PreOrdem(arvore);
    printf("\n Imprimir em pos-ordem: ");
    ARVOREBINARIA_Imprimir_PosOrdem(arvore);
    printf("\n Imprimir em em-ordem: ");
    ARVOREBINARIA_Imprimir_EmOrdem(arvore);

    return 0;
}
