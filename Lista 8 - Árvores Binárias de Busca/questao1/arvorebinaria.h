#ifndef ARVOREBINARIA_H_INCLUDED
#define ARVOREBINARIA_H_INCLUDED
#include <stdio.h>
#include <stdlib.h>

struct arvorebinaria
{
  int valor;
  struct arvorebinaria* pai;
  struct arvorebinaria* dir;
  struct arvorebinaria* esq;
};

typedef struct arvorebinaria ArvoreBinaria;

/** Inseri o elemento na arvore */
ArvoreBinaria* ARVOREBINARIA_InserirElemento(ArvoreBinaria* arvore, int elemento);

/** Cria arvore vazia*/
ArvoreBinaria* ARVOREBINARIA_CriarArvoreVazia();

/** Busca elemento na arvore */
ArvoreBinaria* ARVOREBINARIA_BuscarElemento(ArvoreBinaria* arvore, int elemento);

/** Busca o sucessor em uma arvore, que o menor elemento maior que elemento especificado */
ArvoreBinaria* ARVOREBINARIA_Sucessor(ArvoreBinaria* arvore);

/** Remove determinado elemento da arvore */
ArvoreBinaria* ARVOREBINARIA_Remover(ArvoreBinaria* arvore, int elemento);

/** Imprime a arvore em pre-ordem */
void ARVOREBINARIA_Imprimir_PreOrdem(ArvoreBinaria* arvore);

/** Imprime a arvore em pos-ordem */
void ARVOREBINARIA_Imprimir_PosOrdem(ArvoreBinaria* arvore);

/** Imprime a arvore em em-ordem */
void ARVOREBINARIA_Imprimir_EmOrdem(ArvoreBinaria* arvore);

#endif // ARVOREBINARIA_H_INCLUDED
